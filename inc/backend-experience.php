<?php
//JMC - backend-experience

// JMC- unregister widgets selectively
 function unregister_default_widgets() {
     unregister_widget('WP_Widget_Calendar');
     unregister_widget('WP_Widget_Categories');
     unregister_widget('WP_Widget_Meta');
     unregister_widget('WP_Widget_Archives');
     unregister_widget('WP_Widget_Recent_Posts');
     unregister_widget('WP_Widget_Recent_Comments');
     unregister_widget('WP_Widget_RSS');
     unregister_widget('WP_Widget_Tag_Cloud');
 }
 add_action('widgets_init', 'unregister_default_widgets', 11);

//* -JMC-Replace WordPress login logo with your own
add_action('login_head', 'b3m_custom_login_logo');
function b3m_custom_login_logo() {
echo '<style type="text/css">
h1 a { background-image:url('.get_stylesheet_directory_uri().'/images/login.png) !important; background-size: 250px 150px !important;height: 150px !important; width: 250px !important; margin-bottom: 0 !important; padding-bottom: 0 !important; }
.login form { margin-top: 10px !important; }
</style>';
}
// JMC changing the logo link https://www.wpbeginner.com/wp-tutorials/how-to-change-the-login-logo-url-in-wordpress/
add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
    return 'https://plasterdog.com';
}
// JMC- custom footer message
function modify_footer_admin () {
  echo 'Themed and configured by <a href="http://plasterdog.com">plasterdog web design</a>. ';
  echo 'CMS Powered by<a href="http://WordPress.org"> WordPress !</a>';
}
add_filter('admin_footer_text', 'modify_footer_admin');

// JMC Remove WordPress Widgets from Dashboard Area
function remove_wp_dashboard_widgets(){
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
    remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog (News)
}
add_action('wp_dashboard_setup', 'remove_wp_dashboard_widgets');
//Remove  WordPress Welcome Panel
remove_action('welcome_panel', 'wp_welcome_panel');
// JMC - change the standard wordpress greeting
add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );

function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
$profile_url = get_edit_profile_url( $user_id );

if ( 0 != $user_id ) {
/* Add the "My Account" menu */
$avatar = get_avatar( $user_id, 28 );
$howdy = sprintf( __('Start editing, %1$s'), $current_user->display_name );
$class = empty( $avatar ) ? '' : 'with-avatar';

$wp_admin_bar->add_menu( array(
'id' => 'my-account',
'parent' => 'top-secondary',
'title' => $howdy . $avatar,
'href' => $profile_url,
'meta' => array(
'class' => $class,
),
) );

}
}

// JMC - COMBINING BOTH CUSTOM WIDGETS INTO A SINGLE WIDGET
add_action('wp_dashboard_setup', 'my_dashboard_widgets');

function my_dashboard_widgets() {
     global $wp_meta_boxes;
     unset(
          $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'],
          $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'],
          $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']
     );

add_meta_box( 'dashboard_custom_feed', 'Welcome to your customized site!', 'dashboard_custom_feed_output', 'dashboard', 'side', 'high' );
}
function dashboard_custom_feed_output() {
     echo '<div class="rss-widget">';
     echo '<p>Your site has been significantly customized and many functions exist only in your theme, so think twice before changing it!</p>
 <p>Have a question? contact Jeff McNear by email: <a href="mailto:jeff@plasterdog.com">here</a>. </p>
<p>Old school? give me a call at: 847/849-7060</p>
<p>For a list of tutorials <a href="http://plasterdog.com/category/wordpress-tutorials/" target="_blank">Follow this link</a>
<p><strong>Here are some recent tutorials:</strong></p><hr/>';
     wp_widget_rss_output(array(
          'url' => 'http://plasterdog.com/category/wordpress-tutorials/feed/',
          'title' => 'MY_FEED_TITLE',
          'items' => 5,
          'show_summary' => 0,
          'show_author' => 0,
          'show_date' => 0
     ));

     echo '</div>';
}