<?php
/*
*Template Name: Film Clip Page
 * @package plasterdog
 */

get_header(); ?>
	<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header"><h1><?php the_title(); ?></h1></header><!-- .entry-header -->
	<div class="entry-content">
		<?php the_content(); ?>
<div class="filmclip-array">
<?php $args = array(
	'post_type'            	=> array( 'filmclip_type' ),
	'post_status'            	=> array( 'publish' ),
	'category_name' 	=> get_field('category_slug_name'), 
	'posts_per_page' 	=> -1,
	'order'		=> 'DESC'
);
// The Query
$query_filmclips = new WP_Query( $args );

// The Loop
if ( $query_filmclips->have_posts() ) {
	while ( $query_filmclips->have_posts() ) {
		$query_filmclips->the_post(); ?>
<li>		
		<a href="<?php the_field('url_to_video'); ?>"><img src="<?php the_field('video_thumbnail_url'); ?>"/></a>
		<h3><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
		<?php the_excerpt(); ?>
		<p class="detail-link"><a href="<?php the_permalink(); ?>" rel="bookmark">more details</a></p>
</li>
<?php	}
} else {
	// no posts found
}
// Restore original Post Data
wp_reset_postdata();
?>
</div><!-- ends filmclip array -->
</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
	<?php endwhile; // end of the loop. ?>
	</main><!-- #main -->
	</div><!-- #primary -->
	<div id="secondary" class="widget-area front-book-array" role="complementary">
			<?php if ( ! dynamic_sidebar( 'sidebar-3' ) ) : ?>
			<?php endif; // end sidebar widget area ?>	
	</div><!-- #secondary -->
<?php get_footer(); ?>
