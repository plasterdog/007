<?php
/**
 * plasterdog functions and definitions
 *
 * @package plasterdog
 */
/**
 * Set the content width based on the theme's design and stylesheet.
 */
require_once( __DIR__ . '/inc/customizer-styles.php');
require_once( __DIR__ . '/inc/simplify-profiles.php');
require_once( __DIR__ . '/inc/widget-styling.php');
require_once( __DIR__ . '/inc/backend-experience.php');
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}
if ( ! function_exists( 'plasterdog_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function plasterdog_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on plasterdog, use a find and replace
	 * to change 'plasterdog' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'plasterdog', get_template_directory() . '/languages' );
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'plasterdog' ),
	) );
	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'plasterdog_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
	) );
}
endif; // plasterdog_setup
add_action( 'after_setup_theme', 'plasterdog_setup' );
/**
 * Register widgetized area and update sidebar with default widgets.
 */
function plasterdog_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Archive & Post Sidebar', 'plasterdog' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>'
	) );

        register_sidebar( array(
        'name'          => __( 'Regular Page Sidebar', 'plasterdog' ),
        'id'            => 'sidebar-3',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );

    register_sidebar( array(
        'name'          => __( 'Feed Page Sidebar', 'plasterdog' ),
        'id'            => 'sidebar-2',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );
}
add_action( 'widgets_init', 'plasterdog_widgets_init' );
/**
 * Enqueue scripts and styles.
 */
function plasterdog_scripts() {
	
	wp_enqueue_style( 'reset-styles', get_template_directory_uri() . '/css/reset-styles.css',false,'1.1','all');

	wp_enqueue_style( 'plasterdog-style', get_stylesheet_uri() );

	wp_enqueue_script( 'plasterdog-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'plasterdog-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'plasterdog_scripts' );

// JMC- https://devanswers.co/enqueue-google-fonts-to-wordpress-functions-php/

function google_fonts() {
	$query_args = array(
		'family' => 'Exo:500,500i,600,600i|Roboto:400,400i,500,500i',
		'subset' => 'latin,latin-ext'
	);
	wp_enqueue_style( 'google_fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );
}
add_action('wp_enqueue_scripts', 'google_fonts');




/**
 * Implement the Custom Header feature. - JMC ACTIVATED
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
//JMC => additional image size
function pw_add_image_sizes() {
    add_image_size( 'large-thumb', 300, 300, true );
}
add_action( 'init', 'pw_add_image_sizes' );
 
function pw_show_image_sizes($sizes) {
    $sizes['large-thumb'] = __( 'Custom Thumb', 'pippin' );
    return $sizes;
}
add_filter('image_size_names_choose', 'pw_show_image_sizes');
//* JMC --* Enable support for Post Thumbnails	 */
	add_theme_support( 'post-thumbnails' );
//JMC remove anchor link from more tag
function remove_more_anchor($link) {
     $offset = strpos($link, '#more-');
     if ($offset) {
          $end = strpos($link, '"',$offset);
     }
     if ($end) {
          $link = substr_replace($link, '', $offset, $end-$offset);
     }
     return $link;
}
add_filter('the_content_more_link', 'remove_more_anchor');
//https://css-tricks.com/snippets/wordpress/make-archives-php-include-custom-post-types/
function namespace_add_custom_types( $query ) {
  if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array(
     'post', 'nav_menu_item', 'filmclip_type'
        ));
      return $query;
    }
}
add_filter( 'pre_get_posts', 'namespace_add_custom_types' );
