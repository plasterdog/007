<?php
/**
 * The Template for displaying all single posts.
 *
 * @package plasterdog
 */

get_header(); ?>
  <div id="page" class="hfeed site">
  <div id="content" class="site-content" >
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
    <?php while ( have_posts() ) : the_post(); ?>
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
  <div class="entry-meta"></div><!-- .entry-meta -->
  </header><!-- .entry-header -->
  <div class="entry-content">
<h1><?php the_title(); ?></h1>  
<div class="first-third">
  <div class="first-third-video">
<a href="<?php the_field('url_to_video'); ?>"><img src="<?php the_field('video_thumbnail_url'); ?>"/>
<button class="btn"><i class="fas fa-play"></i></button></a>
</div>
<!-- THIS BLOCK OF CODE SETS THE LINKAGE AND SPECIFIES THE POST TYPE OF THE RELATIONAL LINKS -->
          <?php
          /*
          *  Loop through post objects (assuming this is a multi-select field) ( setup postdata )
          *  Using this method, you can use all the normal WP functions as the $post object is temporarily initialized within the loop
          *  Read more: http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
          */
          $post_objects = get_field('link_to_related_content');
          if( $post_objects ): ?>
            <h3>Related Content</h3>
              <ul>
              <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                  <?php setup_postdata($post); ?>
                  <li>
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <!--https://wordpress.stackexchange.com/questions/169504/how-to-get-current-get-post-types-name-->            
                        (<?php 
                        $postType = get_post_type_object(get_post_type());
                        if ($postType) {
                            echo esc_html($postType->labels->singular_name);
                        }
                         ?>)
                  </li>
              <?php endforeach; ?>
              </ul>
              <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
          <?php endif;
          /*
          *  Loop through post objects (assuming this is a multi-select field) ( don't setup postdata )
          *  Using this method, the $post object is never changed so all functions need a seccond parameter of the post ID in question.
          */
          $post_objects = get_field('post_objects');
          if( $post_objects ): ?>
              <ul>
              <?php foreach( $post_objects as $post_object): ?>
                  <li>
                      <a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a>                      
                  </li>
              <?php endforeach; ?>
              </ul>
          <?php endif; ?>
<!-- finding the category name https://wordpress.stackexchange.com/questions/108570/how-to-display-categories-of-my-custom-post-type-->
<?php

$taxonomy = 'category';
$terms = get_terms($taxonomy); // Get all terms of a taxonomy

if ( $terms && !is_wp_error( $terms ) ) :
?>
    
        <?php foreach ( $terms as $term ) { ?>
          Movie Name:  <?php echo $term->name; ?>
        <?php } ?>
    
<?php endif;?>

<!-- ENDS THE RELATIONAL LINK SECTION -->
</div><!-- ENDS FIRST THIRD-->
<div class="two-thirds">
<?php the_content(); ?>
</div><!-- ENDS TWO THIRDS-->
<div class="clear"> </div>
  </div><!-- .entry-content -->
  <footer class="entry-footer"><?php edit_post_link( __( 'Edit', 'plasterdog' ), '<span class="edit-link">', '</span>' ); ?></footer><!-- .entry-footer -->
</article><!-- #post-## -->
      <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() ) :
          comments_template();
        endif;
      ?>
    <?php endwhile; // end of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->
  <div id="secondary" class="widget-area" role="complementary">
  <header class="page-header"></header>
     <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
    <?php endif; // end sidebar widget area ?>
  </div><!-- #secondary -->
<?php get_footer(); ?>