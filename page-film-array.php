<?php
/**
 * Template Name: Film Array
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package plasterdog
 */

get_header(); ?>
	<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">				
			</header><!-- .entry-header -->
			<div class="entry-content">				
			<?php the_content(); ?>
			<!-- THIS IS THE REPEATER FIELD  -->
				<?php
				// check if the repeater field has rows of data
				if( have_rows('film_thumbnail_repeater') ): ?>
				<ul class="film-array-list">
				<?php 	// loop through the rows of data
				    while ( have_rows('film_thumbnail_repeater') ) : the_row(); ?>
					<li>
							<a href="<?php the_sub_field('film_title_&_thumbnail_link');?>"><img src="<?php the_sub_field('film_thumbnail_image');?>"/></a>
							<a href="<?php the_sub_field('film_title_&_thumbnail_link');?>"><?php the_sub_field('film_title');?></a>
					</li>
		       		<?php    endwhile; ?>
				</ul>
				<?php else :
				    // no rows found
				endif;
				?>
			</div><!-- .entry-content -->	
	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
			<?php endwhile; // end of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->
  <div id="secondary" class="widget-area" role="complementary">  
     <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
    <?php endif; // end sidebar widget area ?>
  </div><!-- #secondary -->
<?php get_footer(); ?>
