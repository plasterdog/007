<?php
/**
 * The Template for displaying all single posts.
 *
 * @package plasterdog
 */

get_header(); ?>
  <div id="page" class="hfeed site">
  <div id="content" class="site-content" >
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
    <?php while ( have_posts() ) : the_post(); ?>
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
  <div class="entry-meta"></div><!-- .entry-meta -->
  </header><!-- .entry-header -->
  <div class="entry-content">
    <h1 class="responsive-page-title"><?php the_title(); ?></h1>      
<div class="movie-detail-container">
<h1 class="post-title"><?php the_title(); ?></h1>
<?php the_content(); ?>
<!-- THIS IS THE REPEATER FIELD FOR THE MOVIE DETAIL SECTION -->
<ul class="just-archive-array">
    <?php
    // check if the repeater field has rows of data
    if( have_rows('movie_detail_repeater') ): ?>
    <?php   // loop through the rows of data
        while ( have_rows('movie_detail_repeater') ) : the_row(); ?>
  <li>
    <h3><?php the_sub_field('movie_detail_title');?></h3>
    <img src=" <?php the_sub_field('movie_detail_thumbnail');?>  "/>
    <p style="text-align:left; margin:0 .5em;"><?php the_sub_field('movie_detail_text');?>   </p>
  </li>
    <?php    endwhile;
    else :
        // no rows found
    endif;
    ?>
</ul><!-- ends archive array-->
<div class="clear"></div>
</div><!-- ends movie detail container -->
  </div><!-- .entry-content -->
  <footer class="entry-footer">
    <?php edit_post_link( __( 'Edit', 'plasterdog' ), '<span class="edit-link">', '</span>' ); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-## -->
        <div class="clear"><!-- variation from default nav which restricts navigation within category -->
<div class="left-split-nav"><?php //previous_post_link('%link', '&larr; %title', TRUE) ?></div>
<div class="right-split-nav"><?php //next_post_link('%link', '%title &rarr;', TRUE) ?></div>
</div>
      <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() ) :
          comments_template();
        endif;
      ?>
    <?php endwhile; // end of the loop. ?>
    </main><!-- #main -->
  </div><!-- #primary -->
  <div id="secondary" class="widget-area" role="complementary">
  <header class="page-header"></header>
     <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
    <?php endif; // end sidebar widget area ?>
  </div><!-- #secondary -->
<?php get_footer(); ?>