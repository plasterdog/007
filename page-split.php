<?php
/**
 * Template Name: Split Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package plasterdog
 */

get_header(); ?>			
	<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">				
			</header><!-- .entry-header -->
			<div class="entry-content">
				<div class="left-side">
				<?php the_content(); ?>
			</div><!-- ends left side -->
			<div class="right-side">
				<?php the_field('right_section'); ?>				
			</div><!-- ends right side-->
			</div><!-- .entry-content -->
			<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
			</article><!-- #post-## -->
			<?php endwhile; // end of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
