<?php
/**
 * Template Name: Home Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package plasterdog
 */

get_header(); ?>			
	<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">				
			</header><!-- .entry-header -->
			<div class="entry-content">
				<?php the_content(); ?>

				<ul class="front-four-features">
				<!-- FIRST TOP FEATURE -->	
				<li>
					<a href="<?php the_field('first_element_link'); ?>">
						<div class="four-background">
						<img src="<?php the_field('first_background_image'); ?>"/>
							<div class="four-title"><img src="<?php the_field('first_title_image'); ?>"/></div><!-- ends four title"-->
						</div><!-- ends four background -->
					</a>
				</li>
				<!-- SECOND TOP FEATURE -->	
				<li>
					<a href="<?php the_field('second_element_link'); ?>">
						<div class="four-background">
						<img src="<?php the_field('second_background_image'); ?>"/>
							<div class="four-title"><img src="<?php the_field('second_title_image'); ?>"/></div><!-- ends four title"-->
						</div><!-- ends four background -->
					</a>
				</li>
				<!-- THIRD TOP FEATURE -->	
				<li>
					<a href="<?php the_field('third_element_link'); ?>">
						<div class="four-background">
						<img src="<?php the_field('third_background_image'); ?>"/>
							<div class="four-title"><img src="<?php the_field('third_title_image'); ?>"/></div><!-- ends four title"-->
						</div> <!-- ends four background -->
					</a>
				</li>
				<!-- FOURTH TOP FEATURE -->	
				<li>
					<a href="<?php the_field('fourth_element_link'); ?>">
						<div class="four-background">
						<img src="<?php the_field('fourth_background_image'); ?>"/>
							<div class="four-title"><img src="<?php the_field('fourth_title_image'); ?>"/></div><!-- ends four title"-->
						</div><!-- ends four background -->
					</a>
				</li>	
			</ul><!-- ends four front features -->	
			<?php endwhile; // end of the loop. ?>

			<ul class="front-four-features">
			<!-- FIRST BOTTOM FEATURE -->			
			<li>
			<?php $args = array(
			'post_type'            	=> array( 'tour_type' ),
			'post_status'            	=> array( 'publish' ),
			'posts_per_page' 	=> 1,
			'order'		=> 'DESC'
			);
			// The Query
			$query_filmclips = new WP_Query( $args );
			// The Loop
			if ( $query_filmclips->have_posts() ) {
			while ( $query_filmclips->have_posts() ) {
			$query_filmclips->the_post(); ?>	
			<!-- finding the category name https://wordpress.stackexchange.com/questions/108570/how-to-display-categories-of-my-custom-post-type -->
			<?php
			$taxonomy = 'category';
			$terms = get_terms($taxonomy); // Get all terms of a taxonomy
			if ( $terms && !is_wp_error( $terms ) ) :
			?>
        	<?php foreach ( $terms as $term ) { ?>				
			<div class="four-sections">
			<a href="<?php the_permalink(); ?>" rel="bookmark">	<h2>Latest Tour</h2></a>		
            <a href="<?php the_permalink(); ?>" rel="bookmark"><h4><?php echo $term->name; ?>
       		<?php } ?>
			<?php endif;?>: <?php the_title(); ?></h4></a>
			<?php	}
			} else {
			// no posts found
			} ?>				
			<p><?php the_excerpt(); ?></p>
			<p class="detail-link"><a href="<?php the_permalink(); ?>" rel="bookmark">more details</a></p>
			<!-- Restore original Post Data -->
			<?php wp_reset_postdata(); ?>
			</div> </li>					
		<!-- SECOND BOTTOM FEATURE -->	
			<li>
			<?php $args = array(
			'post_type'            	=> array( 'filmclip_type' ),
			'post_status'            	=> array( 'publish' ),
			'posts_per_page' 	=> 1,
			'order'		=> 'DESC'
			);
			// The Query
			$query_filmclips = new WP_Query( $args );
			// The Loop
			if ( $query_filmclips->have_posts() ) {
			while ( $query_filmclips->have_posts() ) {
			$query_filmclips->the_post(); ?>	
			<!-- finding the category name https://wordpress.stackexchange.com/questions/108570/how-to-display-categories-of-my-custom-post-type -->
			<?php
			$taxonomy = 'category';
			$terms = get_terms($taxonomy); // Get all terms of a taxonomy
			if ( $terms && !is_wp_error( $terms ) ) :
			?>
        	<?php foreach ( $terms as $term ) { ?>
			<div class="four-sections">	
			<a href="<?php the_permalink(); ?>" rel="bookmark"><h2>Latest Film Clip</h2></a>	
            <a href="<?php the_permalink(); ?>" rel="bookmark"><h4><?php echo $term->name; ?>
       		<?php } ?>
			<?php endif;?>: <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></h4></a>
			<?php	}
			} else {
			// no posts found
			} ?>				
			<p><a href="<?php the_field('url_to_video'); ?>"><img src="<?php the_field('video_thumbnail_url'); ?>"/></a></p>
			<p class="detail-link"><a href="<?php the_permalink(); ?>" rel="bookmark">more details</a></p>
			<!-- Restore original Post Data -->
			<?php wp_reset_postdata(); ?>		
			</div> </li>
		<!-- THIRD BOTTOM FEATURE -->	
			<li>
			<?php $args = array(
			'post_type'            	=> array( 'podcast_type' ),
			'post_status'            	=> array( 'publish' ),
			'posts_per_page' 	=> 1,
			'order'		=> 'DESC'
			);
			// The Query
			$query_filmclips = new WP_Query( $args );
			// The Loop
			if ( $query_filmclips->have_posts() ) {
			while ( $query_filmclips->have_posts() ) {
			$query_filmclips->the_post(); ?>		
			<!-- finding the category name https://wordpress.stackexchange.com/questions/108570/how-to-display-categories-of-my-custom-post-type -->
			<?php
			$taxonomy = 'category';
			$terms = get_terms($taxonomy); // Get all terms of a taxonomy
			if ( $terms && !is_wp_error( $terms ) ) :
			?>
        	<?php foreach ( $terms as $term ) { ?>				
			<div class="four-sections">	
			<a href="<?php the_permalink(); ?>" rel="bookmark"><h2>Latest Podcast</h2></a>
            <a href="<?php the_permalink(); ?>" rel="bookmark"><h4><?php echo $term->name; ?>
       		 <?php } ?>
			<?php endif;?>: <?php the_title(); ?></h4></a>
			<?php	}
			} else {
			// no posts found
			} ?>				
			<?php the_excerpt(); ?>
			<p class="detail-link"><a href="<?php the_permalink(); ?>" rel="bookmark">more details</a></p>
			<!-- Restore original Post Data -->
			<?php wp_reset_postdata(); ?>		
			</div> </li>
		<!-- FOURTH BOTTOM FEATURE -->
			<li>
			<div class="four-sections">	
			<?php the_field('bottom_free_form_section'); ?>	
			</div></li>
</ul>

<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>					</div><!-- .entry-content -->
			
			</article><!-- #post-## -->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
