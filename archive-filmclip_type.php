<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package plasterdog
 */

get_header(); ?>



		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

	<h1 class="responsive-page-title"><?php the_title(); ?></h1>	

			<?php /* Start the Loop */ ?>
			<ul class="just-archive-array">
			<?php global $query_string;
query_posts( $query_string.'&order=DESC' );	?>
			<?php while ( have_posts() ) : the_post(); ?>


<!-- POST FIELDS-->

<li>


	
<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
<div class="entry-content">  

	<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark">filmclip:<?php the_title(); ?></a></h1>	
	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
	

	</div>	<!-- ends entry content -->
</article><!-- #post-## -->



<?php   } else { ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  

	<div class="entry-content">
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<small><?php echo get_the_date(); ?></small>
		</header><!-- .entry-header -->
		<?php the_excerpt(); ?>
		<p align="right" style="margin-bottom:.5em;"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the full article</a></p>
	</div><!-- .entry-content -->
 <?php    } ?>

</article><!-- #post-## -->

</li>
	

	<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
	
	<?php endif; ?>

<!-- ENDS POST FIELDS -->

			<?php endwhile; ?>
</ul><!-- ends archive array-->
			<?php plasterdog_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

	<div id="secondary" class="widget-area" role="complementary">
		<header class="page-header">
				
	

<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
					
					
					<?php endif; // end sidebar widget area ?>		
						
				
								
				
			</header><!-- .page-header -->
		

	</div><!-- #secondary -->

<?php get_footer(); ?>
